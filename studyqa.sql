-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Мар 03 2019 г., 16:31
-- Версия сервера: 5.7.19
-- Версия PHP: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `studyqa`
--

-- --------------------------------------------------------

--
-- Структура таблицы `events`
--

CREATE TABLE `events` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_user` bigint(20) UNSIGNED NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `events`
--

INSERT INTO `events` (`id`, `id_user`, `description`, `created_at`, `updated_at`) VALUES
(1, 2, 'New Lection in September ', '2019-03-01 13:07:33', '2019-03-01 13:07:33'),
(2, 2, 'New Lection in October ', '2019-03-01 13:07:41', '2019-03-01 13:07:41'),
(3, 2, 'New Lection in November', '2019-03-01 13:08:58', '2019-03-01 13:08:58'),
(4, 2, 'New Lection in December', '2019-03-01 13:09:15', '2019-03-01 13:09:15'),
(5, 3, 'Exam in June', '2019-03-01 13:09:15', '2019-03-01 13:09:15'),
(6, 3, 'Exam in July', '2019-03-01 13:09:15', '2019-03-01 13:09:15'),
(7, 3, 'Exam in August', '2019-03-01 13:11:17', '2019-03-01 13:11:17'),
(8, 3, 'Exam in May', '2019-03-01 13:11:24', '2019-03-01 13:11:24');

-- --------------------------------------------------------

--
-- Структура таблицы `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `queue` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_03_01_115216_create_type_users_table', 1),
(4, '2019_03_01_115538_create_study_level_table', 1),
(5, '2019_03_01_115740_create_events_table', 1),
(6, '2019_03_01_120622_create_requests_table', 1),
(7, '2019_03_01_124700_add_type_id_to_users', 2),
(9, '2019_03_03_054548_add_fields_to_requests', 3),
(10, '2019_03_03_070359_create_jobs_table', 4),
(11, '2019_03_03_070609_create_failed_jobs_table', 4);

-- --------------------------------------------------------

--
-- Структура таблицы `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `requests`
--

CREATE TABLE `requests` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `surname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telephone_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_level` bigint(20) UNSIGNED NOT NULL,
  `id_event` bigint(20) UNSIGNED NOT NULL,
  `ip` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `utm` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `requests`
--

INSERT INTO `requests` (`id`, `name`, `surname`, `email`, `telephone_number`, `id_level`, `id_event`, `ip`, `utm`, `created_at`, `updated_at`) VALUES
(8, 'Дмитрий', 'Вязовский', 'dmitriy.vyazovskiy@gmail.com', '5055095340', 1, 1, '127.0.0.1', 'utm_referer: http://studuqa/\n', '2019-03-03 09:31:34', '2019-03-03 09:31:34'),
(9, 'Dmytro', 'Viazovskyi', 'dimcha1978@ukr.net', '5055095340', 1, 1, '127.0.0.1', 'utm_referer: http://studuqa/\n', '2019-03-03 09:32:11', '2019-03-03 09:32:11'),
(11, 'Дмитрий', 'Петров', 'petrov@gmail.com', '5055095304', 3, 5, '127.0.0.1', 'utm_referer: http://studuqa/\n', '2019-03-03 09:40:17', '2019-03-03 09:40:17'),
(13, 'Петр', 'Иванов', 'ivanov@gmail.com', '5055095304', 1, 5, '127.0.0.1', 'utm_referer: http://studuqa/\n', '2019-03-03 09:41:06', '2019-03-03 09:41:06'),
(16, 'Александр', 'Жидков', 'zhidrov@gmail.com', '5055005304', 3, 2, '127.0.0.1', 'utm_referer: http://studuqa/\n', '2019-03-03 09:41:57', '2019-03-03 09:41:57'),
(20, 'Евгений', 'Козлов', 'kozlov@gmail.com', '5059005304', 5, 6, '127.0.0.1', 'utm_referer: http://studuqa/\n', '2019-03-03 09:42:47', '2019-03-03 09:42:47'),
(21, 'Дмитрий', 'Топов', 'topov@gmail.com', '5055095347', 3, 4, '127.0.0.1', 'utm_referer: http://studuqa/\n', '2019-03-03 09:50:11', '2019-03-03 09:50:11'),
(23, 'Дмитрий', 'Изотопов', 'isotopov@gmail.com', '5055095349', 1, 4, '127.0.0.1', 'utm_referer: http://studuqa/\n', '2019-03-03 09:51:12', '2019-03-03 09:51:12'),
(28, 'Том', 'Томов', 'tomov@gmail.com', '5055095348', 1, 4, '127.0.0.1', 'utm_referer: http://studuqa/\n', '2019-03-03 10:01:38', '2019-03-03 10:01:38'),
(29, 'Dmytro', 'Tigipko', 'tigipko@ukr.net', '17560559139', 1, 2, '127.0.0.1', 'utm_referer: http://studuqa/\n', '2019-03-03 10:22:35', '2019-03-03 10:22:35'),
(30, 'Dmytro', 'Tigipko', 'tigipko@ukr.net', '17560559139', 1, 6, '127.0.0.1', 'utm_referer: http://studuqa/\n', '2019-03-03 10:23:05', '2019-03-03 10:23:05'),
(31, 'Dmytro', 'Kozlovich', 'kozlovich@ukr.net', '17560559135', 5, 8, '127.0.0.1', 'utm_referer: http://studuqa/\n', '2019-03-03 10:23:46', '2019-03-03 10:23:46');

-- --------------------------------------------------------

--
-- Структура таблицы `study_level`
--

CREATE TABLE `study_level` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `level_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `study_level`
--

INSERT INTO `study_level` (`id`, `level_name`, `created_at`, `updated_at`) VALUES
(1, 'Bachelor', '2019-03-01 13:02:56', '2019-02-28 21:00:00'),
(3, 'Master', '2019-03-01 13:03:51', '2019-03-01 13:03:51'),
(5, 'PhD', '2019-03-01 13:04:12', '2019-03-01 13:04:12');

-- --------------------------------------------------------

--
-- Структура таблицы `type_users`
--

CREATE TABLE `type_users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `type_users`
--

INSERT INTO `type_users` (`id`, `type_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', NULL, NULL),
(3, 'organisator', NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_type` bigint(20) UNSIGNED DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `id_type`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin@admin.com', NULL, '$2y$10$zSx79.PYHM0/uFn7xGQzXOHhje1fv/el/u67dvnghYTlE2xIyIAC.', 1, NULL, '2019-03-01 09:44:12', '2019-03-01 09:44:12'),
(2, 'Organisator1', 'org1@com', NULL, '$2y$10$aXLO7xWRGthrwOaUmpY5y.NZzC34h3GzFwgsE/Gqllx3zaHE5/ycW', 3, 'LDQ0JQWuX2nfc6nZ95dx8auXwH2QHoQFR8AYVS0JFoztCaVqh3Yya9MOHPyo', '2019-03-01 09:45:13', '2019-03-01 09:45:13'),
(3, 'Organisator2', 'org2@com', NULL, '$2y$10$NIDSJVxIu9pd9YKWC6NwGug9QWdnx8HV8C1pWWBRIo9vHLtJCyyii', 3, NULL, '2019-03-01 09:45:53', '2019-03-01 09:45:53');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`),
  ADD KEY `events_id_user_foreign` (`id_user`);

--
-- Индексы таблицы `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jobs_queue_index` (`queue`);

--
-- Индексы таблицы `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Индексы таблицы `requests`
--
ALTER TABLE `requests`
  ADD PRIMARY KEY (`id`),
  ADD KEY `requests_id_level_foreign` (`id_level`),
  ADD KEY `requests_id_event_foreign` (`id_event`);

--
-- Индексы таблицы `study_level`
--
ALTER TABLE `study_level`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `type_users`
--
ALTER TABLE `type_users`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_id_type_foreign` (`id_type`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `events`
--
ALTER TABLE `events`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT для таблицы `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;
--
-- AUTO_INCREMENT для таблицы `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT для таблицы `requests`
--
ALTER TABLE `requests`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT для таблицы `study_level`
--
ALTER TABLE `study_level`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `type_users`
--
ALTER TABLE `type_users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `events`
--
ALTER TABLE `events`
  ADD CONSTRAINT `events_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `requests`
--
ALTER TABLE `requests`
  ADD CONSTRAINT `requests_id_event_foreign` FOREIGN KEY (`id_event`) REFERENCES `events` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `requests_id_level_foreign` FOREIGN KEY (`id_level`) REFERENCES `study_level` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_id_type_foreign` FOREIGN KEY (`id_type`) REFERENCES `type_users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
