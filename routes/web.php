<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'UserController@getAllEvents');
Route::post('/saverequest', 'UserController@saveRequest');
Route::get('/admin', 'AdminController@getAllRequests')->name('admin');
Route::delete('/deleterequest', 'AdminController@deleteRequests')->name('delete');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
