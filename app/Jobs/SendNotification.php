<?php

namespace App\Jobs;

use App\Notifications\EmailNewRequestOrganisator;
use App\Notifications\EmailNewRequestParticipant;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class SendNotification implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $email;
    protected $type;
    protected $idRequest;
    public function __construct($email, $type, $idRequest)
    {
        $this->email = $email;
        $this->type = $type;
        $this->idRequest = $idRequest;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
       if (strcasecmp($this->type,"participant")==0){
           $emailLetter = new EmailNewRequestParticipant($this->idRequest);
       }
       else $emailLetter = new EmailNewRequestOrganisator($this->idRequest);
       Mail::to($this->email)->send($emailLetter);
    }
}
