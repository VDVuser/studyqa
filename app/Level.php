<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Level extends Model
{
    protected $table = 'study_level';
    protected $primaryKey = 'id';
}
