<?php

namespace App\Notifications;


use App\EventRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Config;

class EmailNewRequestParticipant extends Mailable {
    use Queueable, SerializesModels;

    /**
     * @var \App\User|null
     */
    protected $idRequest;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($idRequest) {

        $this->idRequest=$idRequest;


    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {
        $eventRequest=EventRequest::find($this->idRequest);

        return $this->subject('Регистрация заявки!')->view('email.participant')->with([
            'eventRequest' => $eventRequest]);
    }

}