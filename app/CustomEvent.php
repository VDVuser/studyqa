<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomEvent extends Model
{
    protected $table = 'events';
    protected $primaryKey = 'id';

    public function user(){
        return $this->belongsTo(User::class, 'id_user')
            ->select('users.name', 'users.id');

    }
    public function userEmail(){
        return $this->belongsTo(User::class, 'id_user')
            ->select('users.email');

    }

}
