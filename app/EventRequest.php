<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventRequest extends Model
{
    protected $table = 'requests';
    protected $primaryKey = 'id';

    public function level(){
        return $this->belongsTo(Level::class, 'id_level');

    }
    public function event(){
        return $this->belongsTo(CustomEvent::class, 'id_event');

    }
}
