<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserType extends Model
{
    protected $table = 'type_users';
    protected $primaryKey = 'id';
}
