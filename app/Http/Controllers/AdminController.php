<?php

namespace App\Http\Controllers;

use App\EventRequest;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getAllRequests(){
        $user = Auth()->user();

        if (strcasecmp($user->type->type_name, "admin")==0){
            $allRequestsAdmin =EventRequest::all();
            return view ('admin.admin')->with (['requests'=>$allRequestsAdmin]);

        }
        else {
           $allRequestsOrganisator =EventRequest::all();

           return view ('admin.organisator')->with (['requests'=>$allRequestsOrganisator]);
        }


    }

    public function deleteRequests (Request $request){
        $response =['code'=>204];
        if ($request->has('id')){
            $id = $request->input('id');
            EventRequest::find($id)->delete();
            $response['code']=200;
        }
        return json_encode($response);
    }
}
