<?php

namespace App\Http\Controllers;

use App\CustomEvent;
use App\EventRequest;
use App\Jobs\SendNotification;
use App\Level;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function getAllEvents(){

      $events = CustomEvent::all();
      $levels = Level::all();

      return view('welcome')->with(['events'=>$events, 'levels'=>$levels]);
    }

    public function saveRequest(Request $request){
        $response =['code'=>204];
        if ($request->has('id')&&$request->has('name')&&
            $request->has('surname')&&$request->has('phone')
            &&$request->has('level')&&$request->has('email')){

            $name=$request->input('name');
            $surname=$request->input('surname');
            $phone=$request->input('phone');
            $email = $request->input('email');
            $id = $request->input('id');
            $level = $request->input('level');
            $ip = $_SERVER['REMOTE_ADDR'];
            $utm = $this->get_utm();
            //create new eventRequest
            $newRequest = new EventRequest;
            $newRequest->name=$name;
            $newRequest->surname=$surname;
            $newRequest->email = $email;
            $newRequest->telephone_number = $phone;
            $newRequest->id_event = $id;
            $newRequest->id_level = $level;
            $newRequest->ip=$ip;
            $newRequest->utm = $utm;
            $newRequest->save();
            $idRequest =$newRequest->id;
            //sendmail to participant
            $oJob = (new SendNotification($email, 'participant',$idRequest))->onQueue('low');
            $this->dispatch($oJob);
            //find organisator email
            $emailOrganisator = CustomEvent::find($id)->userEmail->email;
            //sendmail to organisator
            $oJob = (new SendNotification($emailOrganisator, 'organisator',$idRequest))->onQueue('low');
            $this->dispatch($oJob);
            $response['code']=200;

        }

        return json_encode($response);
    }

    function get_utm()
    {

    // если мы не знаем реферала
        if (!isset($_COOKIE['referer'])) {
            // то запоминаем его
            setcookie('referer', $_SERVER['HTTP_REFERER'], time() + 30);
            $utm_referer = $_SERVER['HTTP_REFERER'];
            // А это реферер, если знаем куки и реферер вообще в наличии
        } else {
            $utm_referer = $_COOKIE['referer'];
        }

    // запоминаем utm-метки
        $utm="";
        if (isset($_GET['utm_source']) && !isset($_COOKIE['utm_source'])) {
            foreach ($_GET as $key => $val) {
                if (0 === strpos($key, 'utm')) {
                    setcookie($key, $val, time() + 30);
                    $utm .= "$key: $val\n";
                }
            }
        } else {
            // Выводим все массивы в переменную $utm_result, если есть куки
            foreach ($_COOKIE as $key_utm => $val_utm) {
                if (0 === strpos($key_utm, 'utm')) {
                    $utm .= "$key_utm: $val_utm\n";
                }
            }
        }

        // Добавляем рефера и проверяем если значение не пустое
        if ($_SERVER['HTTP_REFERER'] != '') $utm .= "utm_referer: $utm_referer\n";

        // фильтруем
        $utm_result = htmlspecialchars($utm, ENT_QUOTES, 'UTF-8');

        // Выводим весь результат
        return $utm_result;
    }
}
