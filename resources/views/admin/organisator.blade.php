@extends('layouts.app')
@section('content')

    <div class="container ">


       @if (isset($requests))
        <table class="table table-bordered table-hover">
            <thead class="thead-light">
            <tr>
                <th scope="col">Мероприятие</th>
                <th scope="col">Имя</th>
                <th scope="col">Фамилия</th>
                <th scope="col">телефон</th>
                <th scope="col">Email</th>
                <th scope="col">Уровень образования</th>
                <th scope="col">Дата</th>
            </tr>
            </thead>


            <tbody>

        @foreach($requests as $sRequest)

            @if(Auth()->user()->getAuthIdentifier()==$sRequest->event->user->id)
            <tr>
                <td>{{$sRequest->event->description}}</td>
                <td>{{$sRequest->name}}</td>
                <td>{{$sRequest->surname}}</td>
                <td>{{$sRequest->telephone_number}}</td>
                <td>{{$sRequest->email}}</td>
                <td>{{$sRequest->level->level_name}}</td>
                <td>{{$sRequest->created_at}}</td>

            </tr>
            @endif
        @endforeach

            </tbody>
        </table>
      @endif
    </div>


@endsection