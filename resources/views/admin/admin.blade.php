@extends('layouts.app')
@section('content')

    <div class="container-fluid">
        <table id="resultTable" class="table table-bordered table-hover">
            <thead  class="thead-light">
            <tr>
                <th scope="col">Мероприятие</th>
                <th scope="col">Имя</th>
                <th scope="col">Фамилия</th>
                <th scope="col">телефон</th>
                <th scope="col">Email</th>
                <th scope="col">Уровень образования</th>
                <th scope="col">IP</th>
                <th scope="col">UTM</th>
                <th scope="col">Действие</th>
            </tr>
            </thead>


        <tbody>
        @foreach($requests as $sRequest)
            <tr id="{{"tr".$sRequest->id}}">
                <td>{{$sRequest->event->description}}</td>
                <td>{{$sRequest->name}}</td>
                <td>{{$sRequest->surname}}</td>
                <td>{{$sRequest->telephone_number}}</td>
                <td>{{$sRequest->email}}</td>
                <td>{{$sRequest->level->level_name}}</td>
                <td>{{$sRequest->ip}}</td>
                <td>{{$sRequest->utm}}</td>
                <td><a data-id ="{{$sRequest->id}}" class="delete btn btn-danger">Удалить</a></td>

            </tr>

        @endforeach

        </tbody>
        </table>

    </div>

@include('layouts.responsewindow')
@endsection