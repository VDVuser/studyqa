@extends('layouts.app')
@section('content')

<div class="container ">




    @if (isset($events))
        <div class="row">
        @foreach ($events as $sEvent)
        <div style="margin-bottom: 3%" class="col-sm-6 col-lg-6">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">{{$sEvent->description}}</div>
                </div>

                <div class="card-body">
                    <div class="row">

                        <div  class="col-sm-6 col-lg-6">

                            <a style="width: 100%" data-rel="lightcase" href = "{{asset('/storage/1.jpeg')}}">
                                <img class="d-block w-100" src="{{asset('/storage/1.jpeg')}}">
                            </a>

                        </div>


                        <div class="col-sm-6 col-lg-6">

                            <div class="row">
                                <p>Организатор: {{$sEvent->user->name}}</p>
                            </div>

                            <div style="position: absolute; bottom: 0px;" class="row">
                                <button data-desc="{{json_encode($sEvent)}}" class="more btn btn-success">Участвовать</button>
                            </div>

                        </div>

                    </div>
                </div>
            </div>

        </div>

            @endforeach
        </div>

   @endif


</div>
<div class="modal fade" id="setRequest" tabindex="-1" role="dialog" data-toggle="modal" aria-labelledby="modalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalTitle">Размещение заявки в мероприятии <span id="eventDescription"></span></h5>
                <button type="button" class="close closeButton" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="requestForm">
                <div id="requestBody" class="modal-body">

                    <div id="nameDiv"class="form-group">
                        <input id="name" type="text" class="form-control"
                               placeholder="Имя" required autofocus>
                        <div style="display: none" class="alert-danger" for="name" id="nameMessage">Некорректное имя!</div>
                    </div>
                    <div id="surnameDiv" class="form-group">
                        <input id="surname" type="text" class="form-control"
                               placeholder="Фамилия" required="required">
                        <div style="display: none" class="alert-danger" for="surname" id="surnameMessage">Некорректная фамилия!</div>
                    </div>
                    <div class="form-group">
                        <input id="email" type="email" class="form-control" placeholder="Электронный адрес" required="required">
                    </div>
                    <div id ="phoneDiv" class="form-group">
                        <input id="phone" type="number" min=0 class="form-control"
                               placeholder="Телефон" required="required">
                        <div style="display: none" class="alert-danger" for="phone" id="phoneMessage">Некорректный телефон!Вводить от 10 до 20 цифр!</div>
                    </div>
                    <div class="form-group">
                        <select id = "level" class="form-control">
                            @foreach($levels as $sLevel)
                                <option value = "{{$sLevel->id}}">{{$sLevel->level_name}}</option>
                            @endforeach

                        </select>
                    </div>


                </div>

                <div class="modal-footer">
                    <button id="cancelButton"type="button" class="closeButton btn btn-secondary" data-dismiss="modal">Закрыть</button>
                    <button  id="confirmButton" type="submit" class=" btn btn-primary">Подтвердить</button>
                </div>
            </form>
        </div>

    </div>
</div>
@include('layouts.responsewindow')
@endsection