<div class="modal fade" id="setResponse" tabindex="-1" role="dialog" data-toggle="modal" aria-labelledby="modalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalTitle">Результат выполнения операции</h5>
                <button type="button" class="close closeButton" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div id="responseBody" class="modal-body">

            </div>

            <div class="modal-footer">
                <button id="cancelButton"type="button" class="closeButton btn btn-secondary" data-dismiss="modal">OK</button>
            </div>

        </div>

    </div>
</div>
