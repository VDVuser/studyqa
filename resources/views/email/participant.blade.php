<h1>Добрый день, {{$eventRequest->name.' '.$eventRequest->surname}}!</h1>
<h3>Вы зарегистрировали заявку на участие в мероприятии "{{$eventRequest->event->description}}" !</h3>
<h3>Организатор - {{$eventRequest->event->user->name}}</h3>
<h3>Спасибо за Вашу заявку! Оставайтесь с нами!</h3>