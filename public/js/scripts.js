/**
 * Created by User on 02.03.2019.
 */
"use strict";



$(document).ready(function(){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    let idEvent;
    $(".more").click(function(event){

        let eventObj = JSON.parse(event.currentTarget.dataset.desc);
        let description =eventObj['description'];
        idEvent = eventObj['id'];
        $("#eventDescription").text(description);
        $('#setRequest').modal();

    });//more click
    $('#confirmButton').click(function(e){
        $(".alert-danger").hide();
        let form = $('#requestForm');
        let name = $("#name").val();
        let surname = $("#surname").val();
        let email = $("#email").val();
        let phone = $("#phone").val();
        let level =$("#level").val();

        if( form[0].checkValidity()) {
            e.preventDefault();
            let patternLetters=/^[а-яА-ЯёЁa-zA-Z]+$/;
            let patternPhone = /^[0-9]{10,20}$/;
            let allFieldsValid =true;
            if (!patternLetters.test(name)){
                $('#nameMessage').show();
                allFieldsValid = false;
            }
            if (!patternLetters.test(surname)){
                $('#surnameMessage').show();
                allFieldsValid = false;
            }
            if (!patternPhone.test(phone)){
                $('#phoneMessage').show();
                allFieldsValid = false;
            }
            if (allFieldsValid){
                $.ajax({
                    method:'POST',
                    url:'/saverequest',
                    data:{
                        id:idEvent,
                        name:name,
                        surname:surname,
                        phone:phone,
                        level:level,
                        email:email
                    },
                    success:function(data){
                        $(".closeButton").click();
                        let result = JSON.parse(data);
                        $("#setResponse").modal();
                        $(".message").remove();
                        if (result['code']==200){
                            $("#responseBody").append(`<h3 class="message alert-success">Заявка успешно зарегистрирована!</h3>`);
                        }
                        else {
                            $("#responseBody").append(`<h3 class="message alert-danger">Заявка не зарегистрирована!</h3>`);
                        }

                    },//success
                    error:function () {
                        $(".closeButton").click();
                        $("#setResponse").modal();
                        $(".message").remove();
                        $("#responseBody").append(`<h3 class="message alert-danger">Произошла ошибка сервера!Заявка не зарегистрирована!</h3>`);
                    }//error


                });//ajax


            }//if valid

        }
        else {

            form.find(':submit').click(function(){

            });

        }//else



    });//confirmButton

    $('a[data-rel^=lightcase]').lightcase();

    $('.delete').click(function () {
        let id =$(this).data('id');

        $.ajax({
            method:'DELETE',
            url:'/deleterequest',
            data:{
                id:id
            },
            success:function(data){
                let result = JSON.parse(data);
                $("#setResponse").modal();
                $(".message").remove();
                if (result['code']==200){
                    let adId = "tr"+id.toString();
                    $('tr[id=' + adId + ']').remove();
                    $("#responseBody").append(`<h3 class="message alert-success">Удаление успешно!</h3>`);
                }
                else {
                    $("#responseBody").append(`<h3 class="message alert-danger">Не удалось удалить заявку!</h3>`);
                }

            },//success
            error:function () {
                $("#setResponse").modal();
                $(".message").remove();
                $("#responseBody").append(`<h3 class="message alert-danger">Произошла ошибка сервера!Не удалось удалить заявку!</h3>`);
            }//error


        });//ajax
    });
});